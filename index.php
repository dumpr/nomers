
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="ru-RU">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="ru-RU">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="ru-RU">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="ru-RU">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>С автомобилем круглый год</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<script type='text/javascript' src='jquery-1.7.2.min.js'></script>
</head>

<body class="home page page-id-395 page-template-default singular two-column right-sidebar">

	<div id="header_block">
	<div id="topblock">
	<div class="center_block">
	<div id="site-logo">

	  <div class="erm-main-menu-wrapper">
              <ul>
		<li><a id="logo" href="http://365cars.ru/" title="С автомобилем каждый день"></a></li>
		<li><a class="fav" title="В избранное" href="" rel="sidebar" onclick="return add_favorite(this);"></a></li>
		<li><a id="site-dog" href="http://365cars.ru/raspechatat-blank-dogovora-kupli-prodazhi-avto" title="Распечатать договор купли-продажи">Распечатать договор</a></li>
		<li><a id="site-dog" href="http://365cars.ru/ekzamen-pdd-onlayn" title="Пройти онлайн экзамен ПДД">Онлайн экзамен ПДД</a></li>
		<li><!--noindex--><a id="site-dog" href="http://365cars.ru/contacts" title="Контакты">Контакты</a><!--/noindex--></li>
		<li><!--noindex--><a id="site-dog" href="http://365cars.ru/itogi-konkursov" title="Итоги конкурсов">Конкурсы</a><!--/noindex--></li>
                </ul>

		<div class="erm-languages-wrapper">
		  		</div>
	  </div>
	</div>








</div>
</div>
<div class="center_block">
<header id="branding" role="banner">
		<nav id="access" role="navigation">
				<div class="assistive-text">Главное меню</div>
								<div class="skip-link"><a class="assistive-text" href="#content" title="Перейти к основному содержимому">Перейти к основному содержимому</a></div>
				<div class="skip-link"><a class="assistive-text" href="#secondary" title="Перейти к дополнительному содержимому">Перейти к дополнительному содержимому</a></div>
								<div class="menu"><ul><li class="page_item page-item-14227"><a href="http://365cars.ru/reklama" rel="nofollow"><!--noindex-->Реклама на сайте<!--/noindex--></a></li></ul></div>
			</nav><!-- #access -->

	</header><!-- #branding -->

</div>

</div>

<div id="page" class="hfeed">
	<div id="main">
		<div id="primary">
			<div id="content" role="main">










<div id="informer">
<?php include('informer.php'); ?>
</div>










						<!--noindex-->
	<div id="comments">



</div><!-- #comments -->
<!--/noindex-->

			</div><!-- #content -->
		</div><!-- #primary -->
		<div id="secondary" class="widget-area" role="complementary">
			<aside id="search-2" class="widget widget_search"><div class="widget-title">Поиск по сайту</div>	<form method="get" id="searchform" action="http://365cars.ru/">
		<label for="s" class="assistive-text">Поиск</label>
		<input type="text" class="field" name="s" id="s" placeholder="Поиск">
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="Поиск">
	</form>
</aside><aside id="polls-widget-2" class="widget widget_polls-widget"><div class="widget-title">Опросы</div><div id="polls-2" class="wp-polls">
	<form id="polls_form_2" class="wp-polls-form" action="/index.php" method="post">
		<p style="display: none;"><input type="hidden" id="poll_2_nonce" name="wp-polls-nonce" value="798d230858"></p>
		<p style="display: none;"><input type="hidden" name="poll_id" value="2"></p>
		<p style="text-align: center;"><strong>Какие зимние шины для вас лучше?</strong></p><div id="polls-2-ans" class="wp-polls-ans"><ul class="wp-polls-ul">
		<li><input type="radio" id="poll-answer-6" name="poll_2" value="6"> <label for="poll-answer-6">Шипованные</label></li>
		<li><input type="radio" id="poll-answer-7" name="poll_2" value="7"> <label for="poll-answer-7">Нешипованные</label></li>
		</ul><p style="text-align: center;"><input type="button" name="vote" value="  Голосовать   " class="Buttons" onclick="poll_vote(2);"></p><p style="text-align: center;"><a href="#ViewPollResults" onclick="poll_result(2); return false;" title="View Results Of This Poll">Показать результаты</a></p></div>
	</form>
</div>
</aside><aside id="text-9" class="widget widget_text">			<div class="textwidget"></div>
		</aside><aside id="categories-no-desc-2" class="widget widget_categories_no_desc"><div class="widget-title">Разделы</div>
        <ul>
	<li class="cat-item cat-item-6"><a href="http://365cars.ru/category/tovari" title="Выбираем товары для авто">Выбираем товары для авто</a>
</li>
	<li class="cat-item cat-item-9"><a href="http://365cars.ru/category/istoriya" title="История автомобилей">История автомобилей</a>
</li>
	<li class="cat-item cat-item-2"><a href="http://365cars.ru/category/pokupka-i-prodazha" title="Как купить или продать авто">Как купить или продать авто</a>
</li>
	<li class="cat-item cat-item-75"><a href="http://365cars.ru/category/news" title="Новости">Новости</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://365cars.ru/category/obzor" title="Обзоры автомобилей">Обзоры автомобилей</a>
</li>
	<li class="cat-item cat-item-42"><a href="http://365cars.ru/category/vozhdenie" title="Обучение вождению">Обучение вождению</a>
</li>
	<li class="cat-item cat-item-10"><a href="http://365cars.ru/category/reytingi" title="Рейтинги и сравнения">Рейтинги и сравнения</a>
</li>
	<li class="cat-item cat-item-7"><a href="http://365cars.ru/category/remont" title="Ремонт автомобилей">Ремонт автомобилей</a>
</li>
	<li class="cat-item cat-item-11"><a href="http://365cars.ru/category/soveti" title="Советы автолюбителям">Советы автолюбителям</a>
</li>
	<li class="cat-item cat-item-8"><a href="http://365cars.ru/category/tuning" title="Тюнинг автомобилей">Тюнинг автомобилей</a>
</li>
		</ul>
     </aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<div class="widget-title">Новые статьи</div>		<ul>
					<li>
				<a href="http://365cars.ru/istoriya/istoriya-gaz.html">Становление гиганта: история завода ГАЗ</a>
						</li>
					<li>
				<a href="http://365cars.ru/vozhdenie/bezopasnaya-distantsiya-mezhdu-avtomobilyami.html">Учимся правильно соблюдать дистанцию на дороге</a>
						</li>
					<li>
				<a href="http://365cars.ru/istoriya/rolls-royce.html">На волнах истории: развитие компании Rolls-Royce</a>
						</li>
					<li>
				<a href="http://365cars.ru/news/podorozhanie-osago.html">Дорогая страховка — подорожание ОСАГО, как антикризисная мера</a>
						</li>
					<li>
				<a href="http://365cars.ru/istoriya/istoriya-toyota.html">История создания компании Toyota</a>
						</li>
				</ul>
		</aside><aside id="text-2" class="widget widget_text"><div class="widget-title">Книга бесплатно</div>			<div class="textwidget"><!--noindex-->

<div align="center">Введите адрес своей почты и получите бесплатно книгу "Покупаем автомобиль с пробегом - как правильно выбрать и приобрести подержанный автомобиль. Пособие - Инструкция".
</div>
<!--/noindex--></div>
		</aside><aside id="phpbb-2" class="widget widget_phpbb"><div class="widget-title">Наш форум</div><ul class="phpbb">
<li><!--noindex--><a href="http://forum.365cars.ru/viewforum.php?f=28" rel="nofollow">Правила дорожного движения</a>  -&gt; <a href="http://forum.365cars.ru/viewtopic.php?p=1467#p1467" rel="nofollow">Нюансы теоретического экзамена на водительские права</a><!--/noindex--></li>
<li><!--noindex--><a href="http://forum.365cars.ru/viewforum.php?f=4" rel="nofollow">Опыт купли-продажи и обмена</a>  -&gt; <a href="http://forum.365cars.ru/viewtopic.php?p=1466#p1466" rel="nofollow">Форма бланка договора купли-продажи автомобиля</a><!--/noindex--></li>
<li><!--noindex--><a href="http://forum.365cars.ru/viewforum.php?f=4" rel="nofollow">Опыт купли-продажи и обмена</a>  -&gt; <a href="http://forum.365cars.ru/viewtopic.php?p=1465#p1465" rel="nofollow">Договор купли-продажи автомобиля</a><!--/noindex--></li>
<li><!--noindex--><a href="http://forum.365cars.ru/viewforum.php?f=24" rel="nofollow">Обсуждаем конкурсы</a>  -&gt; <a href="http://forum.365cars.ru/viewtopic.php?p=1463#p1463" rel="nofollow">Автомобильный планшет Lexand SB7 HD</a><!--noindex--></li>
<li><!--noindex--><a href="http://forum.365cars.ru/viewforum.php?f=24" rel="nofollow">Обсуждаем конкурсы</a>  -&gt; <a href="http://forum.365cars.ru/viewtopic.php?p=1462#p1462" rel="nofollow">Автомобильный планшет Lexand SB7 HD</a><!--noindex--></li>
</ul>
</aside><aside id="text-3" class="widget widget_text"><div class="widget-title">Сайт в социальных сетях</div>			<div class="textwidget"><!--noindex-->
<div align="center">
</div>
<!--/noindex--></div>
		</aside><aside id="text-4" class="widget widget_text"><div class="widget-title">Это интересно</div>			<div class="textwidget"><!--noindex--><div id="DIV_DN_4645"></div><!--/noindex-->
</div>
		</aside>		</div><!-- #secondary .widget-area -->

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">



	</footer><!-- #colophon -->
</div><!-- #page -->
<!--noindex-->
<div id="bottomblock">
			<div id="site-generator">
			<div id="footer">Все права защищены © 2015&nbsp;&nbsp;&nbsp; <strong class="gr">365cars.ru&nbsp;&nbsp;&nbsp;</strong>Автомобильный сайт для владельцев машин<a href="http://365cars.ru/karta-sajta">.</a>

		</div>
		<div id="logof">
</div></div></div>

<script>
    $('#informer').on('click','#result>div',function(){
        $(this).select();
    }).on('click','button',function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        $('#result').remove();
        $.post( "infogen.php", $(form).serialize(), function(data) {
            //var data = $.parseJSON(data);
            console.log(data);
            $(form).after(data.block);
            $(form).prev().attr('src',data.src);
        });
    });
</script>
</body>

</html>