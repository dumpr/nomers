<?php

include 'uk_default.php';

return array(
    'uk_1' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/03/d3/6862c27d0de32cb1c037f9c40e2a.png',
            'bg' => 'uk/1.png',
            'h' => 'як у мене',
                ),
        'items' => array(
            'default_region',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(),
            array(
                    'box' => array(58,1,156,156),
                    ),
            array(
                    'box' => array(71,1,156,156),
                    ),
            array(
                    'box' => array(84,1,156,156),
                    ),
            array(
                    'box' => array(97,1,156,156),
                    ),
            array(
                    'box' => array(115,1,156,156),
                    ),
            array(
                    'box' => array(132,1,156,156),
                    ),
            ),
        ),
    'uk_2' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/e7/77/fec9ccd4767465d0b9cc57787faa.png',
            'bg' => 'uk/2.png',
            'h' => 'як був у мене у 1995 році',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_sign',
            'default_digit',
            'default_digit',
            'old_region',
                ),
        'merge' => array(
            array(
                    'box' => array(29,1,156,156),
                    ),
            array(
                    'box' => array(42,1,156,156),
                    ),
            array(
                    'box' => array(56,1,156,156),
                    ),
            array(
                    'box' => array(-3,2,156,156),
                    ),
            array(
                    'box' => array(81,1,156,156),
                    ),
            array(
                    'box' => array(94,1,156,156),
                    ),
            array(
                    'box' => array(52,1,156,156),
                    ),
            array(
                    'box' => array(-61,15,156,156),
                    ),
            ),
        ),
    'uk_3' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/d6/26/96d1d2dd04b41e535a6ac73ea58b.png',
            'bg' => 'uk/3.png',
            'h' => 'як був у мене у 1995 році',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_sign',
            'default_digit',
            'default_digit',
            'old_region',
                ),
        'merge' => array(
            array(
                    'box' => array(29,1,156,156),
                    ),
            array(
                    'box' => array(42,1,156,156),
                    ),
            array(
                    'box' => array(56,1,156,156),
                    ),
            array(
                    'box' => array(-3,2,156,156),
                    ),
            array(
                    'box' => array(81,1,156,156),
                    ),
            array(
                    'box' => array(94,1,156,156),
                    ),
            array(
                    'box' => array(52,1,156,156),
                    ),
            array(
                    'box' => array(-61,15,156,156),
                    ),
            ),
        ),
    'uk_4' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/19/2f/bd50b7691c449a6c96b0236d0e73.png',
            'bg' => 'uk/1.png',
            'h' => 'як у мене (індивідуальний)',
                ),
        'items' => array(
            'default_string',
                ),
        'merge' => array(
            array(),
            ),
        ),
    'uk_5' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/50/17/96c24f4f22029180b8d0e7e53982.png',
            'bg' => 'uk/4.png',
            'h' => 'як у міліції',
                ),
        'items' => array(
            'old_milicia_region',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(-61,15,156,156),
                    ),
            array(
                'box' => array(71,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(87,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(103,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(119,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            ),
        ),
    'uk_6' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/bd/f0/4ea40e205c3a1d6daaf1b8ff40b1.png', // 0001
            'bg' => 'uk/5.png',
            'h' => 'керівництво мвс',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(50,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(66,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(81,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(98,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            ),
        ),
    'uk_7' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/65/26/cd1a3670b7e97323e4017aeb9bd9.png', // 6967 EK
            'bg' => 'uk/6.png',
            'h' => 'як був у міліції у 1995 році',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'old_region',
                ),
        'merge' => array(
            array(
                'box' => array(35,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(50,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(65,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(80,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(46,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(-61,15,156,156),
                'font_color' => array(255,255,255),
                    ),
            ),
        ),
    'uk_8' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/e9/2b/297c04d91e6920c386cd8825532d.png', // 0100 P4
            'bg' => 'uk/7.png',
            'h' => 'як у війскових',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'military',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(25,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(40,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(55,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(70,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(100,0,30,156),
                'font_color' => array(255,255,255),
                    ),
            array(
                'box' => array(125,0,156,156),
                'font_color' => array(255,255,255),
                    ),
            ),
        ),
    'uk_9' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/11/14/68c9c87c77a701758e3510806605.png', // 005 251
            'bg' => 'uk/8.png',
            'h' => 'як у дипломатів',
                ),
        'items' => array(
            'diplomat',
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(),
            array(
                'box' => array(100,0,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(113,0,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(126,0,156,156),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_10' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/d5/c9/874302b26b87d75618be851af4ed.png',
            'bg' => 'uk/9.png',
            'h' => 'як на автобусі',
                ),
        'items' => array(
            'default_region',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(),
            array(
                'box' => array(58,1,156,156),
                    ),
            array(
                'box' => array(71,1,156,156),
                    ),
            array(
                'box' => array(84,1,156,156),
                    ),
            array(
                'box' => array(97,1,156,156),
                    ),
            array(
                'box' => array(116,1,156,156),
                    ),
            array(
                'box' => array(131,1,156,156),
                    ),
            ),
        ),
    'uk_11' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/ed/8a/b33f469587098b44a0c415f30c13.png',
            'bg' => 'uk/10.png',
            'h' => 'Вищі державні установи (стандарт 1995 р.)',
                ),
        'items' => array(
            'gov_1',
            'default_sign',
            'default_digit',
            'default_digit',
            'default_digit',
            'gov_2',
                ),
        'merge' => array(
            array(
                'box' => array(27,-1,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(43,-2,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(69,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(82,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(95,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(100,-1,60,40),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_12' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/f8/87/150780d54fe74efd75ce90c4238f.png', // 3323
            'bg' => 'uk/11.png',
            'h' => 'Урядовий номерний знак (до 2001 р.)',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(47,-1,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(64,-1,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(81,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(98,-1,156,156),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_13' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/ea/89/62d3d66ca4b39276ed0e4f916ebb.png', // 05
            'bg' => 'uk/12.png',
            'h' => 'Урядовий номерний знак (2004—2005 рр., 2 цифри)',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(61,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(79,-1,156,156),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_14' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/10/4f/079c1d638a7ca29d63c4c959be38.png', // 005
            'bg' => 'uk/12.png',
            'h' => 'Урядовий номерний знак (2004—2005 рр., 3 цифри)',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(51,-1,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(68,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(85,-1,156,156),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_15' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/57/1c/7b84b0e855e71355db273a0f2319.png', // 005
            'bg' => 'uk/13.png',
            'h' => 'Урядовий номерний знак (2005—2007 рр.)',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(60,-1,30,40),
                'font_size' => 29,
                    ),
            array(
                'box' => array(77,-1,156,156),
                'font_size' => 29,
                    ),
            array(
                'box' => array(94,-1,156,156),
                'font_size' => 29,
                    ),
            ),
        ),
    'uk_16' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/0f/83/cc6b18a2f8a33a621751cbbd1a2a.png', // A 1578 AH
            'bg' => 'uk/14.png',
            'h' => 'Як був у мене у 1992 році',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'font_face' => 'roadnumbers/RoadNumbers2.0.ttf',
                'box' => array(31,6,30,40),
                'font_size' => 22,
                    ),
            array(
                'box' => array(49,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(63,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(77,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(91,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(111,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(128,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_17' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/0f/c8/05c2f466c2850fe2a22a45e917ff.png', // A 3576 OE
            'bg' => 'uk/15.png',
            'h' => 'Як був у мене у 1992 році (без прапора)',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'font_face' => 'roadnumbers/RoadNumbers2.0.ttf',
                'box' => array(31,6,30,40),
                'font_size' => 22,
                    ),
            array(
                'box' => array(49,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(63,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(77,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(91,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(111,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(128,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_18' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/64/1d/70533ebcf904f6f0192caebd438b.png', // A 4123 KA
            'bg' => 'uk/16.png',
            'h' => 'Як був у мене у 1992 році',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'font_face' => 'roadnumbers/RoadNumbers2.0.ttf',
                'box' => array(16,6,30,40),
                'font_size' => 22,
                    ),
            array(
                'box' => array(37,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(52,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(67,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(82,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(105,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(122,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_19' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/23/bd/16a7267330a1488e0ccc6acf3d3c.png', // 3234 ABB
            'bg' => 'uk/14.png',
            'h' => 'Як був у юр. осіб у 1992 році',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(31,0,30,40),
                'font_size' => 28,
                    ),
            array(
                'box' => array(45,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(59,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(73,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(94,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(111,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(128,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_20' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/16/14/a35b5598f01b6c4499d919bc10cb.png', // 1228 HKM
            'bg' => 'uk/15.png',
            'h' => 'Як був у юр. осіб у 1992 році (без прапора)',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(31,0,30,40),
                'font_size' => 28,
                    ),
            array(
                'box' => array(45,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(59,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(73,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(94,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(111,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(128,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_21' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/45/08/e2aa2892243354cff6f1ec68fe86.png', // 2203 HBA
            'bg' => 'uk/16.png',
            'h' => 'Як був у юр. осіб у 1992 році',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(22,0,30,40),
                'font_size' => 28,
                    ),
            array(
                'box' => array(37,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(52,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(67,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(88,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(105,0,156,156),
                'font_size' => 28,
                    ),
            array(
                'box' => array(122,0,156,156),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_22' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/a2/fa/3f19871a854a00b23f02567c075e.png', // 4060 AB
            'bg' => 'uk/18.png',
            'h' => 'Як був у мене у 1995 році на автобусі',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(12,0,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(26,0,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(40,0,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(54,0,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(68,0,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(43,30,30,30),
                'font_size' => 28,
                    ),
            array(
                'box' => array(62,30,30,30),
                'font_size' => 28,
                    ),
            )
        ),
    'uk_23' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/71/f7/986f94721186e0edeff792073639.png', // aa 2416 be
            'bg' => 'uk/19.png',
            'h' => 'Як на мотоциклі',
                ),
        'items' => array(
            'moto_region',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(40,0,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(27,20,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(41,20,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(55,20,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(69,20,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(40,40,30,30),
                'font_size' => 25,
                    ),
            array(
                'box' => array(54,40,30,30),
                'font_size' => 25,
                    ),
            )
        ),
    'uk_24' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/71/f7/986f94721186e0edeff792073639.png',
            'bg' => 'uk/20.png',
            'h' => 'Як у мене (Америка, Японія)',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(27,0,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(40,0,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(32,20,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(45,20,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(58,20,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(71,20,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(60,0,30,30),
                'font_size' => 22,
                    ),
            array(
                'box' => array(74,0,30,30),
                'font_size' => 22,
                    ),
            )
        ),
    );