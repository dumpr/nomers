<?php

include 'by_default.php';

return array(
    'by_1' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/91/51/e426377108a0d08e997555c99cf2.png', // 1104 HB-9
            'bg' => 'by/1.png',
            'h' => 'как у меня',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_sign',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(31,0,30,40),
                    ),
            array(
                'box' => array(45,0,156,156),
                    ),
            array(
                'box' => array(59,0,156,156),
                    ),
            array(
                'box' => array(73,0,156,156),
                    ),
            array(
                'box' => array(94,0,156,156),
                    ),
            array(
                'box' => array(110,0,156,156),
                    ),
            array(
                'box' => array(116,-2,30,30),
                    ),
            array(
                'box' => array(137,0,156,156),
                    ),
            )
        ),
    'by_2' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/07/01/9c4b8e2151a8b55aa67b383b1a91.png', // вова
            'bg' => 'by/1.png',
            'h' => 'как у меня именной',
                ),
        'items' => array(
            'default_string',
                ),
        'merge' => array(
            array(),
            ),
        ),
    'by_3' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/11/a5/165f9d663413d176919d084a3772.png', // 0650 HKX
            'bg' => 'by/2.png',
            'h' => 'как у меня',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_letter',
                ),
        'merge' => array(
            array(
                'box' => array(12,0,30,40),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(27,0,156,156),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(42,0,156,156),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(57,0,156,156),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(94,0,156,156),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(110,0,156,156),
                'font_color' => array(255,3,0),
                    ),
            array(
                'box' => array(126,0,30,30),
                'font_color' => array(255,3,0),
                    ),
            )
        ),
    'by_4' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/87/fd/8d784a1a51ccb7d237dca82577b0.png',
            'bg' => 'by/1.png',
            'h' => 'грузовые авто и автобусы',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_sign',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(25,0,30,40),
                'font_size' => 27,
                    ),
            array(
                'box' => array(41,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(63,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(77,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(91,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(105,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(47,-4,156,156),
                    ),
            array(
                'box' => array(136,0,156,156),
                'font_size' => 27,
                    ),
            )
        ),
    'by_5' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/11/08/8898d694e937b14e0a950b7badad.png',
            'bg' => 'by/1.png',
            'h' => 'автомобильные прицепы',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_sign',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(25,0,30,40),
                'font_size' => 27,
                    ),
            array(
                'box' => array(49,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(63,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(77,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(91,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(105,0,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(48,-4,156,156),
                    ),
            array(
                'box' => array(136,0,156,156),
                'font_size' => 27,
                    ),
            )
        ),
    'by_6' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/5c/f8/d32ca296b930a34eb5cc6fe54d13.png',
            'bg' => 'by/4.png',
            'h' => 'задний знак грузовиков и автобусов',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_sign',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(24,2,30,40),
                'font_size' => 27,
                    ),
            array(
                'box' => array(40,2,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(45,0,30,30),
                'font_size' => 27,
                    ),
            array(
                'box' => array(66,2,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(24,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(38,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(52,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(66,32,156,156),
                'font_size' => 27,
                    ),
            )
        ),
    'by_7' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/38/19/f2cab93b9b5eb5ca4d98af75fbec.png',
            'bg' => 'by/5.png',
            'h' => 'задний знак легковых авто и прицепов',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_sign',
            'default_digit',
                ),
        'merge' => array(
            array(
                'box' => array(24,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(38,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(52,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(66,32,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(24,2,30,40),
                'font_size' => 27,
                    ),
            array(
                'box' => array(40,2,156,156),
                'font_size' => 27,
                    ),
            array(
                'box' => array(45,0,30,30),
                'font_size' => 27,
                    ),
            array(
                'box' => array(66,2,156,156),
                'font_size' => 27,
                    ),
            ),
        ),
    );