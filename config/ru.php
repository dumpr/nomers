<?php

include 'ru_default.php';

return array(
    'ru_1' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/68/7e/cd3d9c20fc528c9e7fea74dfadb9.png',
            'bg' => 'ru/1.png',
            'h' => 'как у меня',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_region',
                ),
        'merge' => array(
            array(),
            array(),
            array(
                    'box' => array(44,-5,156,156),
                    ),
            array(
                    'box' => array(61,-5,156,156),
                    ),
            array(
                    'box' => array(78,-5,156,156),
                    ),
            array(
                    'box' => array(95,-5,156,156),
                    ),
            array(),
            ),
        ),
    'ru_2' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/57/39/9482fd23157c768b50205b5efe8a.png', // вован
            'bg' => 'ru/1.png',
            'h' => 'как у меня (именной)',
                ),
        'items' => array(
            'default_string',
            'default_region',
                ),
        'merge' => array(
            array(),
            array(),
            ),
        ),
    'ru_3' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/e7/fb/d06d7c3939501194d9a8c867a7c8.png', // A7777 177
            'bg' => 'ru/2.png',
            'h' => 'как у полиции',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_region',
                ),
        'merge' => array(
            array(
                    'font_color' => array(255,255,255),
                    ),
            array(
                    'box' => array(34,-5,156,156),
                    'font_color' => array(255,255,255),
                    ),
            array(
                    'box' => array(51,-5,156,156),
                    'font_color' => array(255,255,255),
                    ),
            array(
                    'box' => array(68,-5,156,156),
                    'font_color' => array(255,255,255),
                    ),
            array(
                    'box' => array(85,-5,156,156),
                    'font_color' => array(255,255,255),
                    ),
            array(
                    'font_color' => array(255,255,255),
                    ),
            ),
        ),
    'ru_4' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/7d/db/e23cebd08eeb8cd73e43dc024e5c.png', // 0001TT 10
            'bg' => 'ru/3.png',
            'h' => 'как у военных',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_region',
                ),
        'merge' => array(
            array(
                'box' => array(10,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'font_color' => array(255,255,255),
            ),
            array(
                'box' => array(44,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(61,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(78,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(95,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'font_color' => array(255,255,255),
                ),
            ),
        ),
    'ru_5' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/de/3f/7a7a8ede072a98a4717b23a86402.png', // 777cc1 77
            'bg' => 'ru/4.png',
            'h' => 'как у дипломатов',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_digit',
            'default_region',
                ),
        'merge' => array(
            array(
                'box' => array(10,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(44,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(61,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(78,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                'box' => array(95,-5,156,156),
                'font_color' => array(255,255,255),
                ),
            array(
                    'font_color' => array(255,255,255),
                ),
            ),
        ),
    'ru_6' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/f8/bb/0fd1f933c1e166f87c897ddca3e2.png', // AA777 177
            'bg' => 'ru/5.png',
            'h' => 'как у автобусов',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_region',
                ),
        'merge' => array(
            array(
                'box' => array(17,-5,156,156),
                ),
            array(
                'box' => array(35,-5,156,156),
                ),
            array(
                'box' => array(56,-5,156,156),
                ),
            array(
                'box' => array(73,-5,156,156),
                ),
            array(
                'box' => array(90,-5,156,156),
                ),
            array(),
            ),
        ),
    'ru_7' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/aa/f4/e88fa44bbd6d14ff274aa949b1c0.png', // AA777 177
            'bg' => 'ru/6.png',
            'h' => 'как у транзитов',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_region',
                ),
        'merge' => array(
            array(
                    'box' => array(31,-5,156,156),
                    ),
            array(
                    'box' => array(48,-5,156,156),
                    ),
            array(
                    'box' => array(65,-5,156,156),
                    ),
            array(
                    'box' => array(82,-5,156,156),
                    ),
            array(
                    'box' => array(99,-5,156,156),
                    ),
            array(),
            ),
        ),
    'ru_8' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/f0/f3/c9ea8b14707ca050f559cec95c72.png', // AA777A 177
            'bg' => 'ru/7.png',
            'h' => 'как у новых транзитов',
                ),
        'items' => array(
            'default_letter',
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_region',
                ),
        'merge' => array(
            array(
                'box' => array(10,-5,156,156),
                ),
            array(
                'box' => array(27,-5,156,156),
                ),
            array(
                'box' => array(44,-5,156,156),
                ),
            array(
                'box' => array(61,-5,156,156),
                ),
            array(
                'box' => array(78,-5,156,156),
                ),
            array(
                'box' => array(95,-5,156,156),
                ),
            array(),
            ),
        ),
    'ru_9' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/51/c1/aefa0b2ce727e10a3cd450c9b974.png', // 0000 AA 01
            'bg' => 'ru/8.png',
            'h' => 'как на мотоцикле',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_region',
                ),
        'merge' => array(
            array(
                'box' => array(11,-5,30,30),
                ),
            array(
                'box' => array(29,-5,30,30),
                ),
            array(
                'box' => array(47,-5,30,30),
                ),
            array(
                'box' => array(65,-5,30,30),
                ),
            array(
                'box' => array(11,28,30,30),
                ),
            array(
                'box' => array(28,28,30,30),
                ),
            array(
                'box' => array(46,38,40,30),
                'font_fase' => 'arial/arialbd.ttf',
                'font_size' => 24,
                ),
            ),
        ),
    );

?>