<?php

include 'kz_default.php';

return array(
    'kz_1' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/f1/a8/953cc13d1a42265c2bb137c82d47.png',
            'bg' => 'kz/1.png',
            'h' => 'как у меня',
                ),
        'items' => array(
            'default_letter',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_letter',
            'default_letter',
            'default_we_letter',
                ),
        'merge' => array(
            array(
                'box' => array(17,0,30,40),
                    ),
            array(
                'box' => array(45,0,156,156),
                    ),
            array(
                'box' => array(59,0,156,156),
                    ),
            array(
                'box' => array(73,0,156,156),
                    ),
            array(
                'box' => array(94,0,156,156),
                    ),
            array(
                'box' => array(110,0,156,156),
                    ),
            array(
                'box' => array(126,0,30,30),
                    ),
            ),
        ),
    'kz_2' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/15/ab/447c5b9390172c9fb120f20e2918.png',
            'bg' => 'kz/1.png',
            'h' => 'как у меня (именной)',
                ),
        'items' => array(
            'default_string',
            'default_sign',
            'default_sign',
                ),
        'merge' => array(
            array(
                    'box' => array(0,0,110,30),
                    ),
            array(
                    'box' => array(100,0,30,30),
                    'sign' => 'K',
                    'font_size' => 28,
                    ),
            array(
                    'box' => array(118,0,30,30),
                    'sign' => 'Z',
                    'font_size' => 28,
                    ),
            ),
        ),
    'kz_3' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/40/a8/242f61fabd3d869bb2a3112b6606.png',
            'bg' => 'kz/4.png',
            'h' => 'минестерство обороны',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_digit',
            'default_digit',
            'default_sign',
            'default_sign',
                ),
        'merge' => array(
            array(
                    'box' => array(39,0,156,156),
                    ),
            array(
                    'box' => array(54,0,156,156),
                    ),
            array(
                    'box' => array(69,0,156,156),
                    ),
            array(
                    'box' => array(84,0,156,156),
                    ),
            array(
                    'box' => array(106,0,30,30),
                    'sign' => 'A',
                    'font_size' => 27,
                    ),
            array(
                    'box' => array(124,0,30,30),
                    'sign' => 'A',
                    'font_size' => 27,
                    ),
            ),
        ),
    'kz_4' => array(
        'main' => array(
            'width' => 156,
            'height' => 36,
            'default' => 'data/55/c7/a83c48769e6b7c7c058a3b5a42a4.png',
            'bg' => 'kz/5.png',
            'h' => 'удп и км рк',
                ),
        'items' => array(
            'default_digit',
            'default_digit',
            'default_sign',
            'default_sign',
                ),
        'merge' => array(
             array(
                    'box' => array(67,0,156,156),
                    ),
            array(
                    'box' => array(84,0,156,156),
                    ),
            array(
                    'box' => array(106,0,30,30),
                    'sign' => 'K',
                    'font_size' => 27,
                    ),
            array(
                    'box' => array(124,0,30,30),
                    'sign' => 'Z',
                    'font_size' => 27,
                    ),
            ),
        ),
    );