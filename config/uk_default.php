<?php

// roadnumbers/RoadNumbers2.0.ttf
// arial/ARIAL.TTF
// arial/arialbd.ttf
// arial/arialnb.ttf
$fontFase = 'arial/arialnb.ttf';

$letters = array(
	'A'=>'A','B'=>'B','C'=>'C','E'=>'E','H'=>'H','I'=>'I','K'=>'K','M'=>'M','O'=>'O','P'=>'P','T'=>'T','X'=>'X');
$region = array(
        'AA' => 'AA',
        'AB' => 'AB',
        'AC' => 'AC',
        'AE' => 'AE',
        'AH' => 'AH',
        'AK' => 'AK',
        'AM' => 'AM',
        'AO' => 'AO',
        'AP' => 'AP',
        'AT' => 'AT',
        'AI' => 'AI',
        'AX' => 'AX',
        'BA' => 'BA',
        'BB' => 'BB',
        'BC' => 'BC',
        'BE' => 'BE',
        'BH' => 'BH',
        'BI' => 'BI',
        'BK' => 'BK',
        'BM' => 'BM',
        'BO' => 'BO',
        'BT' => 'BT',
        'BX' => 'BX',
        'CA' => 'CA',
        'CB' => 'CB',
        'CE' => 'CE',
        'CH' => 'CH',
        'II' => 'II'
	);
$old_region = array(
    'KO' => '01',
    'KP' => '01',
    'PK' => '01',
    'BI' => '02',
    'BT' => '02',
    'BX' => '02',
    'BK' => '03',
    'BM' => '03',
    'BO' => '03',
    'AA' => '04',
    'AB' => '04',
    'AE' => '04',
    'AK' => '04',
    'AH' => '04',
    'AI' => '04',
    'CM' => '04',
    'EA' => '05',
    'EB' => '05',
    'EE' => '05',
    'EK' => '05',
    'EH' => '05',
    'EO' => '05',
    'EC' => '05',
    'BA' => '06',
    'BB' => '06',
    'BE' => '06',
    'PT' => '07',
    'PE' => '07',
    'PP' => '07',
    'HA' => '08',
    'HE' => '08',
    'HO' => '08',
    'HP' => '08',
    'HC' => '08',
    'IB' => '09',
    'IC' => '09',
    'KK' => '10',
    'KM' => '10',
    'KX' => '10',
    'KA' => '11',
    'KB' => '11',
    'KE' => '11',
    'KH' => '11',
    'KI' => '11',
    'KT' => '11',
    'OO' => '11',
    'II' => '11',
    'OM' => '12',
    'OH' => '12',
    'OC' => '12',
    'AM' => '13',
    'AO' => '13',
    'AP' => '13',
    'AT' => '13',
    'AX' => '13',
    'IA' => '13',
    'TA' => '14',
    'TB' => '14',
    'TH' => '14',
    'TC' => '14',
    'TT' => '14',
    'HK' => '15',
    'HT' => '15',
    'HI' => '15',
    'OA' => '16',
    'OB' => '16',
    'OE' => '16',
    'OK' => '16',
    'OT' => '16',
    'CK' => '17',
    'CH' => '17',
    'CC' => '17',
    'PA' => '18',
    'PB' => '18',
    'PO' => '18',
    'CA' => '19',
    'CB' => '19',
    'CE' => '19',
    'TE' => '20',
    'TK' => '20',
    'TI' => '20',
    'XA' => '21',
    'XB' => '21',
    'XK' => '21',
    'XP' => '21',
    'XH' => '22',
    'XO' => '22',
    'XM' => '23',
    'XI' => '23',
    'MA' => '24',
    'MB' => '24',
    'ME' => '24',
    'MT' => '24',
    'MK' => '25',
    'MM' => '25',
    'MH' => '25',
    'MO' => '26',
    'MP' => '26',
    'MC' => '26',
    'KC' => '27',
    'HH' => '28',
    'II' => '28',
);
$old_region_html = '<select name="N[]">
				<optgroup label="АР Крым">
                <option value="KO">KO</option>
                <option value="KP">KP</option>
                <option value="PK">PK</option>
				</optgroup><optgroup label="Винницкая обл.">
                <option value="BI">BI</option>
                <option value="BT">BT</option>
                <option value="BX">BX</option>
				</optgroup><optgroup label="Волынская обл.">
                <option value="BK">BK</option>
                <option value="BM">BM</option>
                <option value="BO">BO</option>
				</optgroup><optgroup label="Днепропетровская обл.">
                <option value="AA">AA</option>
                <option value="AB">AB</option>
                <option value="AE">AE</option>
                <option value="AK">AK</option>
                <option value="AH">AH</option>
                <option value="AI">AI</option>
                <option value="CM">CM</option>
				</optgroup><optgroup label="Донецкая обл.">
                <option value="EA">EA</option>
                <option value="EB">EB</option>
                <option value="EE">EE</option>
                <option value="EK">EK</option>
                <option value="EH">EH</option>
                <option value="EO">EO</option>
                <option value="EC">EC</option>
				</optgroup><optgroup label="Житомирская обл.">
                <option value="BA">BA</option>
                <option value="BB">BB</option>
                <option value="BE">BE</option>
				</optgroup><optgroup label="Закарпатская обл.">
                <option value="PT">PT</option>
                <option value="PE">PE</option>
                <option value="PP">PP</option>
				</optgroup><optgroup label="Запорожская обл.">
                <option value="HA">HA</option>
                <option value="HE">HE</option>
                <option value="HO">HO</option>
                <option value="HP">HP</option>
                <option value="HC">HC</option>
				</optgroup><optgroup label="Ивано-Франковская обл.">
                <option value="IB>IB</option>
                <option value=" ic="">IC</option>
				</optgroup><optgroup label="Киевская обл.">
                <option value="KK">KK</option>
                <option value="KM">KM</option>
                <option value="KX">KX</option>
				</optgroup><optgroup label="г. Киев">
                <option value="KA">KA</option>
                <option value="KB">KB</option>
                <option value="KE">KE</option>
                <option value="KH">KH</option>
                <option value="KI">KI</option>
                <option value="KT">KT</option>
                <option value="OO">OO</option>
                <option value="II">II</option>
				</optgroup><optgroup label="Кировоградская обл.">
                <option value="OM">OM</option>
                <option value="OH">OH</option>
                <option value="OC">OC</option>
				</optgroup><optgroup label="Луганская обл.">
                <option value="AM">AM</option>
                <option value="AO">AO</option>
                <option value="AP">AP</option>
                <option value="AT">AT</option>
                <option value="AX">AX</option>
                <option value="IA">IA</option>
				</optgroup><optgroup label="Львовская обл.">
                <option value="TA">TA</option>
                <option value="TB">TB</option>
                <option value="TH">TH</option>
                <option value="TC">TC</option>
                <option value="TT">TT</option>
				</optgroup><optgroup label="Николаевская обл.">
                <option value="HK">HK</option>
                <option value="HT">HT</option>
                <option value="HI">HI</option>
				</optgroup><optgroup label="Одесская обл.">
                <option value="OA">OA</option>
                <option value="OB">OB</option>
                <option value="OE">OE</option>
                <option value="OK">OK</option>
                <option value="OT">OT</option>
				</optgroup><optgroup label="Полтавская обл.">
                <option value="CK">CK</option>
                <option value="CH">CH</option>
                <option value="CC">CC</option>
				</optgroup><optgroup label="Ровенская обл.">
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PO">PO</option>
				</optgroup><optgroup label="Сумская обл.">
                <option value="CA">CA</option>
                <option value="CB">CB</option>
                <option value="CE">CE</option>
				</optgroup><optgroup label="Тернопольская обл.">
                <option value="TE">TE</option>
                <option value="TK">TK</option>
                <option value="TI">TI</option>
				</optgroup><optgroup label="Харьковская обл.">
                <option value="XA">XA</option>
                <option value="XB">XB</option>
                <option value="XK">XK</option>
                <option value="XP">XP</option>
				</optgroup><optgroup label="Херсонская обл.">
                <option value="XH">XH</option>
                <option value="XO">XO</option>
				</optgroup><optgroup label="Хмельницкая обл.">
                <option value="XM">XM</option>
                <option value="XI">XI</option>
				</optgroup><optgroup label="Черкасская обл.">
                <option value="MA">MA</option>
                <option value="MB">MB</option>
                <option value="ME">ME</option>
                <option value="MT">MT</option>
				</optgroup><optgroup label="Черниговская обл.">
                <option value="MK">MK</option>
                <option value="MM">MM</option>
                <option value="MH">MH</option>
				</optgroup><optgroup label="Черновицкая обл.">
                <option value="MO">MO</option>
                <option value="MP">MP</option>
                <option value="MC">MC</option>
				</optgroup><optgroup label="г. Севастополь">
                <option value="KC">KC</option>
				</optgroup><optgroup label="Общегосударственные">
                <option value="HH">HH</option>
                <option value="IIo">II</option>
            </optgroup></select>';

$old_milicia_html = '<select name="N[]>
                <option value="01"> АР Крым </option>
                <option value="02"> Винницкая обл. </option>
                <option value="03"> Волынская обл. </option>
                <option value="04"> Днепропетровская обл. </option>
                <option value="05"> Донецкая обл. </option>
                <option value="06"> Житомирская обл. </option>
                <option value="07"> Закарпатская обл. </option>
                <option value="08"> Запорожская обл. </option>
                <option value="09"> Ивано-Франковская обл. </option>
		<option value="10"> Киевская обл. </option>
                <option value="11"> г. Киев </option>
                <option value="12"> Кировоградская обл. </option>
                <option value="13"> Луганская обл. </option>
                <option value="14"> Львовская обл. </option>
                <option value="15"> Николаевская обл. </option>
                <option value="16"> Одесская обл. </option>
                <option value="17"> Полтавская обл. </option>
                <option value="18"> Ровненская обл. </option>
		<option value="19"> Сумская обл. </option>
                <option value="20"> Тернопольская обл. </option>
                <option value="21"> Харьковская обл. </option>
                <option value="22"> Херсонская обл. </option>
                <option value="23"> Хмельницкая обл. </option>
                <option value="24"> Черкаская обл. </option>
                <option value="25"> Черниговская обл. </option>
                <option value="26"> Черновицкая обл. </option>
                <option value="27"> г. Севастополь </option>
            </select>';

$military_html = '<select name="N[]" style="width: 280px;">
                <option value="A">А - Министерство обороны </option>
                <option value="B">В - Военная автоинспекция </option>
                <option value="E">Е - Военно-морские силы Украины </option>
                <option value="P">Р - Ремонтные предприятия Министерства обороны </option>
                <option value="C">С - Строительные войска </option>
                <option value="T">Т - Транспортные войска </option>
                <option value="X">Х - Прицепы </option>
                <option value="Z">Ч - Министерство по чрезвычайным ситуациям </option>
            </select>';

$diplomat_html = '<select name="N[]" style="width:60px">
<option value="001">001 - Россия </option>
<option value="002">002 - США </option>
<option value="003">003 - Китай </option>
<option value="004">004 - Великобритания </option>
<option value="005">005 - Франция </option>
<option value="006">006 - Германия </option>
<option value="007">007 - Венгрия </option>
<option value="008">008 - Литва </option>
<option value="009">009 - Болгария </option>
<option value="010">010 - Армения </option>
<option value="011">011 - Австралия </option>
<option value="012">012 - Израиль </option>
<option value="013">013 - Беларусь </option>
<option value="014">014 - Польша </option>
<option value="015">015 - Эстония </option>
<option value="016">016 - резервный код </option>
<option value="017">017 - Швеция </option>
<option value="018">018 - Мексика </option>
<option value="019">019 - Греция </option>
<option value="020">020 - Индия </option>
<option value="021">021 - Иран </option>
<option value="022">022 - Монголия </option>
<option value="023">023 - Вьетнам </option>
<option value="024">024 - Австрия </option>
<option value="025">025 - Египет </option>
<option value="026">026 - Аргентина </option>
<option value="027">027 - Япония </option>
<option value="028">028 - Канада </option>
<option value="029">029 - Португалия </option>
<option value="030">030 - Чили </option>
<option value="031">031 - Италия </option>
<option value="032">032 - Испания </option>
<option value="033">033 - Словакия </option>
<option value="034">034 - Румыния </option>
<option value="035">035 - Турция </option>
<option value="036">036 - Норвегия </option>
<option value="037">037 - Алжир </option>
<option value="038">038 - Швейцария </option>
<option value="039">039 - Нигер </option>
<option value="040">040 - Ватикан </option>
<option value="041">041 - Азербайджан </option>
<option value="042">042 - Корея </option>
<option value="043">043 - Бразилия </option>
<option value="044">044 - Дания </option>
<option value="045">045 - Латвия </option>
<option value="046">046 - Хорватия </option>
<option value="047">047 - Кипр </option>
<option value="048">048 - Чехия </option>
<option value="049">049 - Финляндия </option>
<option value="050">050 - Сейшельские о-ва </option>
<option value="051">051 - ООН </option>
<option value="052">052 - Центрально-Африканская Республика </option>
<option value="053">053 - Словения </option>
<option value="054">054 - Бельгия </option>
<option value="055">055 - Молдова </option>
<option value="056">056 - Куба </option>
<option value="057">057 - Филиппины </option>
<option value="058">058 - ЮАР </option>
<option value="059">059 - Пакистан </option>
<option value="060">060 - Ливия </option>
<option value="061">061 - Международный Банк Реконструкций и Развития (Всемирный Банк) </option>
<option value="062">062 - Международная Финансовая Корпорация </option>
<option value="063">063 - Сирия </option>
<option value="064">064 - Сингапур </option>
<option value="065">065 - Ирландия </option>
<option value="066">066 - Нидерланды </option>
<option value="067">067 - Казахстан </option>
<option value="068">068 - Грузия </option>
<option value="069">069 - Ирак </option>
<option value="070">070 - Кыргызстан </option>
<option value="071">071 - Таджикистан </option>
<option value="072">072 - Узбекистан </option>
<option value="073">073 - Туркменистан </option>
<option value="074">074 - Таиланд </option>
<option value="075">075 - Тунис </option>
<option value="076">076 - Гана </option>
<option value="077">077 - Сербия </option>
<option value="078">078 - Лихтенштейн </option>
<option value="079">079 - Индонезия </option>
<option value="080">080 - Марокко </option>
<option value="081">081 - Иордания </option>
<option value="082">082 - Кувейт </option>
<option value="083">083 - Люксембург </option>
<option value="084">084 - ОБСЕ </option>
<option value="085">085 - Уругвай </option>
<option value="086">086 - Республика Перу </option>
<option value="087">087 - Международная Миссия Красного Креста </option>
<option value="088">088 - Афганистан </option>
<option value="089">089 - Украинский научно-технологический центр (УНТЦ) </option>
<option value="090">090 - ранее принадлежал Югославии </option>
<option value="091">091 - Албания </option>
<option value="092">092 - Македония </option>
<option value="093">093 - Офис связи НАТО </option>
<option value="094">094 - Информационный центр НАТО </option>
<option value="095">095 - Международный Валютный Фонд </option>
<option value="096">096 - Новая Зеландия </option>
<option value="097">097 - Бангладеш </option>
<option value="098">098 - Комиссия Европейского Экономического Сообщества </option>
<option value="099">099 - Европейский Банк Реконструкций и Развития </option>
<option value="100">100 - Генеральная дирекция по обслуживанию иностранных представительств </option>
 </select>';

$moto_html = '<select name="N[]">
<option value="AA">AA</option>
<option value="AB">AB</option>
<option value="AC">AC</option>
<option value="AE">AE</option>
<option value="AH">AH</option>
<option value="AM">AM</option>
<option value="AO">AO</option>
<option value="AP">AP</option>
<option value="AT">AT</option>
<option value="AI">AI</option>
<option value="AK">AK</option>
<option value="BA">BA</option>
<option value="BB">BB</option>
<option value="BC">BC</option>
<option value="BE">BE</option>
<option value="BH">BH</option>
<option value="BI">BI</option>
<option value="BK">BK</option>
<option value="BM">BM</option>
<option value="BO">BO</option>
<option value="AX">AX</option>
<option value="BT">BT</option>
<option value="BX">BX</option>
<option value="CA">CA</option>
<option value="CB">CB</option>
<option value="CE">CE</option>
<option value="CH">CH</option>
<option value="II">II</option>
</select>';

$template = array(
    'default_letter' => array(
        'data' => $letters,
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,-1,156,156),
        'font_size' => 27,
    ),
    'default_digit' => array(
        'data' => array_combine(range(0,9),range(0,9)),
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,0,156,156),
        'font_size' => 27,
    ),
    'default_sign' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,2,110,40),
        'font_size' => 22,
        'sign' => '-',
    ),
    'default_region' => array(
        'data' => $region,
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(8,1,60,30),
        'font_size' => 27,
    ),
    'small_region' => array(
        'data' => $old_region,
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(8,22,60,30),
        'font_size' => 16,
    ),
    'default_string' => array(
        'data' => array(),
        'font_fase' => 'arial/ARIAL.TTF',
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,2,110,40),
        'font_size' => 26,
        'string' => 'шелест',
    ),
    'moto_region' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,-1,110,40),
        'font_size' => 27,
        'html' => $moto_html,
    ),
    'old_region' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,-1,110,40),
        'font_size' => 27,
        'html' => $old_region_html,
    ),
    'old_milicia_region' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(255,255,255),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,-1,110,40),
        'font_size' => 15,
        'html' => $old_milicia_html,
    ),
    'military' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(255,255,255),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,-1,110,40),
        'font_size' => 27,
        'html' => $military_html,
    ),
    'diplomat' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,0,110,40),
        'font_size' => 29,
        'html' => $diplomat_html,
    ),
    'gov_1' => array(
        'data' => array('0'=>'0','1'=>'1','2'=>'2','3'=>'3'),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,0,110,40),
        'font_size' => 29,
    ),
    'gov_2' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,0,110,40),
        'font_size' => 29,
        'html' => '<select name="N[]">
                <option value="AQ">АП </option>
                <option value="KM">KM </option>
		<option value="BP">BP </option>
            </select>',
    ),
);

