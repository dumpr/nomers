<?php

$fontFase = 'arial/arialnb.ttf';

$letters = array(
	'A'=>'A','B'=>'B','C'=>'C','E'=>'E','H'=>'H','I'=>'I','K'=>'K','M'=>'M','O'=>'O','P'=>'P','T'=>'T','X'=>'X');

$template = array(
    'default_letter' => array(
        'data' => $letters,
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(168,18,19,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,0,156,156),
        'font_size' => 28,
    ),
    'default_digit' => array(
        'data' => array_combine(range(0,9),range(0,9)),
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,0,156,156),
        'font_size' => 28,
    ),
    'default_sign' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,2,110,40),
        'font_size' => 28,
        'sign' => '-',
    ),
    'default_string' => array(
        'data' => array(),
        'font_fase' => 'arial/ARIAL.TTF',
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,0,110,40),
        'font_size' => 28,
        'string' => 'ВОВА',
    ),
);

