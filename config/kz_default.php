<?php

$fontFase = 'arial/arialnb.ttf';

$letters = array(
	'A'=>'A','B'=>'B','C'=>'C','E'=>'E','H'=>'H','I'=>'I','K'=>'K','M'=>'M','O'=>'O','P'=>'P','T'=>'T','X'=>'X');

$latin_html = '<select name="N[]">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
                <option value="F">F</option>
                <option value="H">H</option>
                <option value="K">K</option>
                <option value="L">L</option>
                <option value="M">M</option>
                <option value="N">N</option>
                <option value="O">O</option>
                <option value="P">P</option>
                <option value="R">R</option>
                <option value="S">S</option>
                <option value="T">T</option>
                <option value="U">U</option>
                <option value="V">V</option>
                <option value="W">W</option>
                <option value="X">X</option>
                <option value="Y">Y</option>
                <option value="Z">Z</option>
            </select>';

$latin_we_html = '<select name="N[]">
                <option value=" "></option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
                <option value="F">F</option>
                <option value="H">H</option>
                <option value="K">K</option>
                <option value="L">L</option>
                <option value="M">M</option>
                <option value="N">N</option>
                <option value="O">O</option>
                <option value="P">P</option>
                <option value="R">R</option>
                <option value="S">S</option>
                <option value="T">T</option>
                <option value="U">U</option>
                <option value="V">V</option>
                <option value="W">W</option>
                <option value="X">X</option>
                <option value="Y">Y</option>
                <option value="Z">Z</option>
            </select>';

$template = array(
    'default_we_letter' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,-1,156,156),
        'html' => $latin_we_html,
        'font_size' => 27,
    ),
    'default_letter' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(10,-1,156,156),
        'html' => $latin_html,
        'font_size' => 27,
    ),
    'default_digit' => array(
        'data' => array_combine(range(0,9),range(0,9)),
        'font_fase' => $fontFase,
        'text_align' => array('left','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,0,156,156),
        'font_size' => 27,
    ),
    'default_sign' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(4,2,110,40),
        'font_size' => 22,
        'sign' => 'A',
    ),
    'default_string' => array(
        'data' => array(),
        'font_fase' => $fontFase,
        'text_align' => array('center','top'),
        'font_color' => array(0,0,0),
        'text_shadow' => array(
            array(128,128,128,99,1,1),
            array(128,128,128,99,-1,-1),
                ),
        'box' => array(27,0,110,40),
        'font_size' => 28,
        'string' => 'ВОВА',
    ),
);

