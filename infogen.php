<?php

include('Debug.php');

mb_internal_encoding('utf8');

$html = '';
$bbcode = '';

if(isset($_POST['N'])){

    $template_array = explode('_',$_POST['template']);
    $country = (string)$template_array[0];
    $key = (int)$template_array[1];
    $post = $_POST['N'];

    sscanf(md5($country.$key.implode('',$post)), "%2s%2s%s", $one, $two, $three);
    $path = __DIR__.'/data/' . $one . '/' . $two;
    $filesrc = 'data/' . $one . '/' . $two.'/'.$three.'.png';
    if(!file_exists($path.'/'.$three.'.png'))
    {

        $config = require_once('config/'.$country.'.php');

        @mkdir($path, 0777, true);

        if(isset($config[$_POST['template']]))
            $data = $config[$_POST['template']];
        else exit;


        require('GDText/Box.php');
        require('GDText/Color.php');


        $bg_file = __DIR__.'/bg/'.$data['main']['bg'];
        if(!file_exists($bg_file))
            $bg_file = __DIR__.'/bg/'.$country.'/1.png';

        $im = @imagecreatefrompng($bg_file);

        if(array_search('old_region', $data['items'])){
            array_push($data['items'], 'small_region');
            $post[] = $template['small_region']['data'][end($post)];
        }

        foreach($data['items'] as $n=>$v)
        {
            $item = array_merge($template[$v],$data['merge'][$n]);

            $box = new Box($im);
            $box->setFontFace(__DIR__.'/fonts/'.$item['font_fase']);
            $box->setFontColor(new Color($item['font_color'][0], $item['font_color'][1], $item['font_color'][2]));
            foreach($item['text_shadow'] as $shadow){
                $box->setTextShadow(new Color($shadow[0],$shadow[1],$shadow[2],$shadow[3]),$shadow[4],$shadow[5]);
            }
            $box->setFontSize($item['font_size']);
            $box->setBox($item['box'][0], $item['box'][1], $item['box'][2], $item['box'][3]);
            $box->setTextAlign($item['text_align'][0], $item['text_align'][1]);

            $str = $post[$n];
            if($v == 'default_string'){
                $str = preg_replace('/[^a-z0-9а-я]/ui', '',$str); // isU
                $str = mb_substr($str,0,8,'utf8');
                $str = mb_strtoupper($str,'utf8');
            }

            $box->draw($str);

        }

        //header("Content-type: image/png");
        //imagepng($im);

        imagepng($im,$path.'/'.$three.'.png');
    }
    $html = '<a href="http://'.$_SERVER['HTTP_HOST'].'" target="_blank"><img src="http://'.$_SERVER['HTTP_HOST'].'/'.(($_SERVER['HTTP_HOST']=="wp") ? "informer" : 'n').'/'.$filesrc.'" /></a>';
    $bbcode = '[url=http://'.$_SERVER['HTTP_HOST'].'][img]http://'.$_SERVER['HTTP_HOST'].'/'.(($_SERVER['HTTP_HOST']=="wp") ? "informer" : 'n').'/'.$filesrc.'[/img][/url]';

    $return['src'] = $filesrc;
    $return['block'] = '
<div id="result">
    <h4>Для форума</h4>
    <textarea class="forblog">'.htmlentities($html).'</textarea>
    <h4>Для блога</h4>
    <textarea class="forforum">'.htmlentities($bbcode).'</textarea>

</div>';
    header('Content-Type: application/json');
    echo json_encode($return);
} ?>