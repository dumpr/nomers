<?php

include('Debug.php');

/*require_once('../wp-load.php');

define('WP_USE_THEMES', false);
require($_SERVER['DOCUMENT_ROOT']."/wp-blog-header.php");
get_header();*/

?>






<style>
#result>textarea{
    padding: 5px;
    border: dashed 1px;
    background-color: #F4F4F4;
    font-size: 13px;
    font-family: monospace;
    width: 560px;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none
}
#informer form{
    display: inline-block;
    padding: 10px;
    background-color: #40373A
}
#informer select,#informer input{
    border: none;
    height: 28px;
    margin-right: 5px;
    padding: 5px;
    font-weight: bold;
    background: #fff;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none
}
#informer input[type="text"]{
    height:18px
}
#informer button{
    background-color: #F63C3D;
    border: none;
    color: #FFF;
    height: 28px;
    cursor: pointer;
    font-weight: bold;
    padding: 0 20px;
}
#informer h4{
    padding: 10px;
    font-size: 14px
}
#informer img{
    display: block;
    margin-bottom: 10px
}
#informer form span{
    display: inline-block;
    width: 10px;
    color: white;
    text-align: center;
    margin-right: 5px;
    font-weight: bold
}
.etabs{margin:0;padding:0}
.etabs li{margin:0 -3px;text-align:center;width:24%}
.etabs li a{text-decoration:none;color:black}
.etabs li.active{padding-top:0;background-color:black}
.etabs li a.active{font-weight:normal;color:white;background-color:#3F3739}
.tab {display:inline-block;zoom:1;*display:inline;background:#eee;border:solid 1px #999;-moz-border-radius:4px 4px 0 0;-webkit-border-radius:4px 4px 0 0}
.tab a{font-size:14px;line-height:2em;display:block;padding:0 10px;outline:none}
.tab a:hover{text-decoration:underline}
.tab.active{background:#fff;padding-top:6px;position:relative;border-color:#666}
.tab a.active{font-weight:bold}
.tab-container .panel-container{background:#fff;border:solid #666 1px;padding:10px;-moz-border-radius:0 4px 4px 4px;-webkit-border-radius:0 4px 4px 4px}

#tab-container h2{margin-top:30px;text-transform:uppercase}
#tab-container hr{margin:0 0 12px;width:96%;display:block;float:left}
</style>

<script src="jquery.easytabs.min.js" type="text/javascript"></script>

<h1>Информеры с номером автомобиля</h1>

<p>На этой странице Вы сможете создать картинку с номером Вашего автомобиля. Изображение номера Вы сможете использовать в качестве подписи на форумах, сайтах или блогах.</p>
<p>Получить картинку-информер очень просто: наберите номер Вашего авто и регион, зачем нажмите кнопку «Получить». После этого Вы получите новый код, который нужно будет скопировать.</p>

<?php $countries = array('ru'=>'Россия','uk'=>'Украина','by'=>'Беларусь','kz'=>'Казахстан'); //  ?>

<div id="tab-container" class="tab-container">
  <ul class='etabs'>
    <?php foreach($countries as $code => $country) { ?>
    <li class='tab'><a href="#<?= $code ?>"><?= $country ?></a></li>
    <?php } ?>
  </ul>

<?php foreach($countries as $code=>$country) { ?>

    <div id="<?= $code ?>">

    <?php $config = require_once('config/'.$code.'.php');

    foreach($config as $name => $row){ ?>

    <h2><?= $row['main']['h'] ?></h2><hr />

    <img src="<?= $row['main']['default'] ?>" alt="" />
    <form method="post">
        <input type="hidden" name="template" value="<?= $name ?>" />
        <?php foreach ($row['items'] as $n=>$key) {

            $item = array_merge($template[$key],$row['merge'][$n]);
            if(isset($item['sign'])){
                               //qw($item, 'e');
                echo '<span>'.$item['sign'].'</span><input type="hidden" name="N[]" value="'.$item['sign'].'" />';
            } elseif (isset($item['html'])){
                echo $item['html'];
            } elseif (isset($item['string'])){
                echo '<input type="text" name="N[]" value="'.$item['string'].'" />';
            } else {
                echo '<select name="N[]">';
                foreach ($item['data'] as $k => $v){
                    if(isset($post[$n]) && $post[$n]==$k)
                            $selected = ' selected="selected"';
                    else $selected = "";
                    echo '<option value="'.$k.'"'.$selected.'> '.$v.' </option>';
                }
                echo '</select>';
            }
        } ?>
        <button type="submit">Получить</button>
    </form>

    <?php } ?>

    </div>
<?php } ?>

</div>

<script>
    $('#tab-container').easytabs({animationSpeed:'fast',updateHash:false});
    $('#tab-container').on('focus','textarea',function() {
            $(this).select();
	});
</script>


<?php // echo dbginfo();

// get_footer();
?>